class Person:
    def __init__(self, name, age):
        self._name = name
        self._age = age
    
    def getName(self):
        print(f'Name of Person: {p1._name}')
        
    def setName(self, name):
        self._name = name
        
    def getAge(self):
        print(f'Age of {self._name}: {p1._age}')
    
    def setAge(self, age):
        self._age = age
        
p1 = Person("Celso", 22)

p1.getName()
p1.getAge()
p1.setAge(32)
p1.getAge()


class Employee(Person):
    def __init__(self, name, age, employeeId):
        super().__init__(name, age)
        self._employeeId = employeeId
        
    def get_employeeId(self):
        print(f'ID of Employee: {self._employeeId}')
        
    def set_employeeId(self, id):
        self._employeeId = id
    
    def get_details(self):
        print(f'The ID of Employee {self._name} is {self._employeeId}')
    
emp1 = Employee("Bona",20,"Emp-001")
emp1.get_details()

class Student(Person):
    
    def __init__(self, name, age, studNum, course, yr_lvl):
        super().__init__(name, age)
        self._studentNumber = studNum
        self._course = course
        self._yearLevel = yr_lvl
        
    def getStudentNumber(self):
       print(f'ID of Student: {self._studentNumber}')
       
    def getCourse(self):
       print(f'Course of Student: {self._course}')
       
    def getYearLevel(self):
       print(f'Year Level of Student: {self._yearLevel}')
       
    def get_detail(self):
        print(f"{self._name} is currently in year {self._yearLevel} taking up {self._course}")
        

s1 = Student("Celso", 23, "17100626","Short Python Course", 4)
s1.getName()
s1.getAge()
s1.getCourse()
s1.get_detail()

# [Section] Polymorphism
class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print('Admin User')

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print('Customer User')	
def test_function(obj):
	obj.is_admin()
	obj.user_type()

# Create instance of admin and customer
admin_user = Admin()
customer_user = Customer()

test_function(admin_user)
test_function(customer_user)

class TeamLead():
	def occupation(self):
		print('Team Lead')

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print('Team Member')

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1,tm1):
	person.occupation()

# Polymorphism with Inheritance
# Polymorphism i python defines methods
# in child class that have the same name 
# as methods in the parent class

# "Method Overriding"

class Zuitt():
	def tracks(self):
		print("We arae currently offerring 3 tracs(developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours!")

class DeveloperCareer(Zuitt):
	# override the parent's num_of_hours() method

	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours!")

class PiShapedCareer(Zuitt):
	# override the parent's num_of_hours() method

	def num_of_hours(self):
		print("Learn Skills for no-code app development in 140 hours!")

class ShortCourses(Zuitt):
	# override the parent's num_of_hours() method

	def num_of_hours(self):
		print("Learn advanced topics in webdevelopment in 20 hours!")

course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()

