class Animal:
    
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
        
    def get_name(self):
        print(f'Name of Animal: {self._name}')
        
    def set_name(self, name):
        self._name = name
        
    def get_breed(self):
        print(f'Breed of Animal: {self._breed}')
        
    def set_breed(self, breed):
        self._breed = breed
    
    def get_age(self):
        print(f'Age of Animal: {self._age}')
        
    def set_age(self, age):
        self._age = age

    def eat(self, food):
        print(f'The animal ate {food}')
    
    def make_sound(self):
        print(f'Come here animal!!!')
        
class Dog(Animal):
    
    def __init__(self, name, breed, age):
        super().__init__(name, breed, age)
    
    def eat(self, food):
        print(f'Noom noom {food}!')
    
    def make_sound(self):
        print(f'Bork Bork!')
    
    def call(self):
        print("*Whistle!*")
    
class Cat(Animal):
    
    def __init__(self, name, breed, age):
        super().__init__(name, breed, age)
    
    def eat(self, food):
        print(f'Tasty {food}!')
    
    def make_sound(self):
        print(f'Human! I demand food')
    
    def call(self):
        print("Pssst pssst")
    
dog1 = Dog("Carra", "Poodle", 10)
dog1.eat("Chicken")
dog1.make_sound()
dog1.call()

cat1 = Cat("Chomeow", "Stray", 3)
cat1.eat("Fish")
cat1.make_sound()
cat1.call()