from django.urls import path
from . import views


# Syntax
# path(route, view, name)
app_name = 'todolist'
urlpatterns = [
	path('', views.index, name='index'),
	#/todolist/<todoitem_id>
	#/todolist/1
	# The <int:todoitem_id> allows for creating a dynamic link where the todoitem_id is provided
	path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
	# /todolist/register
	path('register', views.register_user, name='register'),
	path('change_password', views.change_password, name="change_password"),
	path('login', views.login_user, name="login"),
	path('logout', views.logout_view, name="logout"),
	path('add_event', views.add_event, name="add_event"),
]