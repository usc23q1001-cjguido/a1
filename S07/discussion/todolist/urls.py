# Created File from discussion

# Syntax
# path(route, view name)

from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index')
]