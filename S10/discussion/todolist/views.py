from django.shortcuts import render, redirect
from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect

from .models import ToDoItem, RegisterForm

def index(request):
	todoitem_list = ToDoItem.objects.all()
	context = {
		'todoitem_list': todoitem_list,
		'user': request.user
		}

	return render(request, "todolist/index.html", context)

	# template = loader.get_template("todolist/index.html")
	# context = {
	# 	'todoitem_list': todoitem_list
	# }
	# return HttpResponse(template.render(context, request))

	# output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
	# return HttpResponse(output)
	# return HttpResponse("Hello from the views.py file")

def todoitem(request, todoitem_id):
	
	todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))

	return render(request, "todolist/todoitem.html", todoitem)


	# response = "You are viewing the details of %s"
	# return HttpResponse(response % todoitem_id)

def register_user(request):
    # if this is a POST request we need to process the form data
    template = 'todolist/register.html'
   
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = RegisterForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            if User.objects.filter(username=form.cleaned_data['username']).exists():
                return render(request, template, {
                    'form': form,
                    'error_message': 'Username already exists.'
                })
            elif User.objects.filter(email=form.cleaned_data['email']).exists():
                return render(request, template, {
                    'form': form,
                    'error_message': 'Email already exists.'
                })
            elif form.cleaned_data['password'] != form.cleaned_data['password_repeat']:
                return render(request, template, {
                    'form': form,
                    'error_message': 'Passwords do not match.'
                })
            else:
                # Create the user:
                user = User.objects.create_user(
                    form.cleaned_data['username'],
                    form.cleaned_data['email'],
                    form.cleaned_data['password']
                )
                user.first_name = form.cleaned_data['first_name']
                user.last_name = form.cleaned_data['last_name']
                user.save()
               
                # Login the user
                login(request, user)
                # return redirect("index")
                # redirect to accounts page:
                return HttpResponseRedirect('/todolist')
				

   # No post data availabe, let's just show the page.
    else:
        form = RegisterForm()

    return render(request, template, {'form': form})


def register(request):
	users = User.objects.all()
	is_user_registered = False
	context = {
	"is_user_registered": is_user_registered
	}

	for indiv_user in users:
		if indiv_user.username == "johndoe":
			is_user_registered = True
			break

	if is_user_registered == False:
		user = User()
		user.username = "johndoe"
		user.first_name = "John"
		user.last_name = "Doe"
		user.email = "john@mail.com"
		# user.password = "john1234"
		# The set_password method is used to ensure that the password is hashed
		user.set_password("john1234")
		user.is_staff = False
		user.is_active = True
		user.save()
		context = {
			"first_name": user.first_name,
			"last_name": user.last_name
		}


	return render(request, "todolist/register.html", context)


def change_password(request):
	is_user_authenticated = False

	user = authenticate(username="johndoe", password="john1234")
	print(user)

	if user is not None:
		# user = User.objects.get(username='johndoe')
		# user.set_password("johndoe1")
		# user.save()
		# is_user_authenticated = True
		authenticated_user = User.objects.get(username='johndoe')
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		"is_user_authenticated": is_user_authenticated
	}

	return render(request, "todolist/change_password.html", context)

def login_view(request):
	username = "johndoe"
	password = "johndoe1"
	user = authenticate(username=username, password=password)
	print(user)

	if user is not None:
		login(request, user)
		return redirect("index")

	else:
		context = {
			"is_user_authenticated": False
		}
		return render(request, "todolist/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("index")
