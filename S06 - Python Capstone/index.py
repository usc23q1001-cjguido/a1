from abc import ABC, abstractclassmethod

class Person(ABC):
    
    def __init__(self, fName, lName, email, department):
        self._firstName = fName
        self._lastName = lName
        self._email = email
        self._department = department
        
    # Getters
    
    def getFirstName(self):
        return self._firstName

    def getLastName(self):
        return self._lastName
    
    def getEmail(self):
        return self._email
    
    def getDepartment(self):
        return self._department
    
    # Setters
    
    def setFirstName(self, value):
        self._firstName = value
        
    def setLastName(self, value):
        self._lastName = value
        
    def setEmail(self, value):
        self._email = value
        
    def setDepartment(self, value):
        self._department = value
    
    # Abstract Methods
    @abstractclassmethod
    def getFullName(self):
        return " ".join([self._firstName, self._lastName])
    
    def addRequest(self):
        return ""
    
    def checkRequest(self):
        return ""
    
    def addUser(self):
        return ""

class Employee(Person):
    
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"
    
    def addRequest(self):
        return "Request has been added"
    
    def checkRequest(self):
        return ""
    
    def addUser(self):
        return "User has been added"
    
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

class TeamLead(Person):
    
    def __init__(self, fName, lName, email, department):
        super().__init__(fName, lName, email, department)
        self._members = []
    
    def getMembers(self):
        return self._members
    
    def setMembers(self, members):
        self._members = members
    
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"
    
    def addRequest(self):
        return ""
    
    def checkRequest(self):
        return ""
    
    def addUser(self):
        return "User has been added"
    
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"
    
    def addMember(self, emp):
        self._members.append(emp)
        
class Admin(Person):
    
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"
    
    def checkRequest(self):
        return ""
    
    def addUser(self):
        return "User has been added"        
        
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"
        
class Request:
    
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "open"
    
    # Getters
    
    def getname(self):
        return self._name
    
    def getRequester(self):
        return self._requester
    
    def getDateRequested(self):
        return self._dateRequested
    
    def getStatus(self):
        return self._status
    
    # Setters
    
    def setName(self, value):
        self._name = value
    
    def setRequester(self, value):
        self._requester = value
    
    def setDateRequested(self, value):
        self._dateRequested = value
        
    def setStatus(self, value):
        self._status = value
        
    # Class Methods
        
    def updateRequest(self):
        return ""
        
    def closeRequest(self):
        self._status = "closed"
        return f"The request {self._name} has been closed"
    
    def cancelRequest(self):
        return ""
    
            
employee1 = Employee("John", "Doe","djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com","Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com","Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New Hire Operation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop Repair", employee1, "1-Jul-2021")


assert employee1.getFullName() == "John Doe", "Full Name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full Name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full Name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"


teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
  print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.setStatus("closed")
print(req2.closeRequest())